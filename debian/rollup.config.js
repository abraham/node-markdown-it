module.exports = [
  {
    input: 'index.js',
    output: {
      file: 'dist/markdown-it.js',
      name: 'markdownit',
      format: 'umd',
    },
  },
];
